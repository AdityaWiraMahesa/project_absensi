import 'dart:convert';
import 'package:project_undangan/model/absen.dart';
import 'package:http/http.dart' as http;


class ApiAbsen {
  final url = 'http://localhost:8080/api_absen/daftar_guru.php';
  Future<List<Absen>?> getAbsenAll() async {
    final response = await http.get(Uri.parse(url));
    if (response.statusCode == 200) {
      return absenFromJson(response.body);
    } else {
      print("Error ${response.toString()}");
      return null;
    }
  }

  //Cek Absen
  Future<Absen?> cek_absen(String id) async {
    final response = await http.get(Uri.parse('http://localhost:8080/api_absen/cek_absen.php?id=$id'));
    if (response.statusCode == 200) {
      final result = json.decode(response.body);
      return Absen.fromJson(result[0]);
    } else {
      print("Error ${response.toString()}");
      return null;
    }
  }

  //Update Kehadiran
  Future<bool> update_absen(Absen absen) async {
    final response = await http.get(Uri.parse('http://localhost:8080/api_absen/update_status.php?id=$id'));
    if (response.statusCode == 200)
      return true;
    else
      return false;
  }

  //Reset Kehadiran
  Future<bool> reset_absen() async {
    final response = await http.get(Uri.parse('http://localhost:8080/api_absen/reset_absen.php'));
    if (response.statusCode == 200)
      return true;
    else
      return false;
  }

}
