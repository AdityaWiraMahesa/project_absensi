import 'dart:convert';

List<Absen> absenFromJson(String str) =>
    List<Absen>.from(json.decode(str).map((x) => Absen.fromJson(x)));

class Absen {
  String id;
  String nama;
  String status;
  String foto;

  Absen({
    required this.id,
    required this.nama,
    required this.status,
    required this.foto,
  });

  factory Absen.fromJson(Map<String, dynamic> json) => Absen(
        id: json["id"],
        nama: json["nama"],
        status: json["status"],
        foto: json["foto"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "nama": nama,
        "status": status,
        "foto": foto,
      };
}
